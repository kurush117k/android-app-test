﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class WatchScript : MonoBehaviour
{
    /// <summary>
    /// Things left to do
    /// -UI
    /// -Error Messages
    /// </summary>
    /// 

    #region Application Variables

    //WatchScript Object
    public static WatchScript m_instance { set; get; }

    //UI
    protected Text m_latitudeUI;
    protected Text m_longitudeUI;
    protected Text m_twitterUserScreenNameUI;

    protected Text m_cameraErrorUI;
    protected Text m_locationErrorUI;
    protected Text m_twitterOnPostErrorUI;
    protected Text m_twitterOnRequestErrorUI;
    protected Text m_twitterOnAccessErrorUI;

    protected InputField m_pinUI;

    //Camera
    protected bool m_camAvailable;
    protected WebCamTexture m_backCam;
    protected Texture m_defaultBackground;
    protected RawImage m_background;
    protected AspectRatioFitter m_fit;

    //GPS Location
    protected float m_latitude;
    protected float m_longitude;

    //Twitter
    const string m_consumerKey = "alm0VrQDVydCdrnMvuY7RjKJp";
    const string m_consumerSecret = "d4t6UxA8vzn7ciKX9FZj05J7XO0BzUauC1L3BZUjWtGvZG3mzK";
    const string m_requestToken = "TwitterRequestTokken";
    const string m_requestTokenSecret = "TwitterRequestTokkenSecret";

    const string m_twitterUserID = "TwitterUserID";
    const string m_twitterUserScreenName = "TwitterUserScreenName";
    const string m_twitterUserToken = "TwitterUserToken";
    const string m_twitterUserTokenSecret = "TwitterUserTokenSecret";

    Twitter.RequestTokenResponse m_requestTokenResponse;
    Twitter.AccessTokenResponse m_accessTokenResponse;

    string m_pin;

    #endregion

    // Use this for initialization
    void Start ()
    {
        Initialization();
        InitializationCamera();
        InitializationLocation();
        InitializationTwitterUser();
        InitializationUI();
	}

    #region Application Initialization

    protected virtual void Initialization()
    {
        m_instance = this;
        DontDestroyOnLoad(gameObject);
    }

    protected virtual void InitializationUI()
    {
        m_latitudeUI = GameObject.Find("Latitude Text").GetComponent<Text>();
        m_longitudeUI = GameObject.Find("Longitude Text").GetComponent<Text>();
        m_twitterUserScreenNameUI = GameObject.Find("Twitter Screen Name Text").GetComponent<Text>();

        m_cameraErrorUI = GameObject.Find("Camera Error Text").GetComponent<Text>();
        m_locationErrorUI = GameObject.Find("Location Error Text").GetComponent<Text>();
        m_twitterOnPostErrorUI = GameObject.Find("Twitter On Post Error Text").GetComponent<Text>();
        m_twitterOnRequestErrorUI = GameObject.Find("Twitter On Request Error Text").GetComponent<Text>();
        m_twitterOnAccessErrorUI = GameObject.Find("Twitter On Access Error Text").GetComponent<Text>();

        m_pinUI = GameObject.Find("Pin Input").GetComponent<InputField>();

        if (m_accessTokenResponse != null)
        {
            if (m_twitterUserScreenNameUI != null)
            {
                if (!string.IsNullOrEmpty(m_accessTokenResponse.ScreenName))
                {
                    m_twitterUserScreenNameUI.text = "User Screen Name: " + m_accessTokenResponse.ScreenName;
                }
                else
                {
                    m_twitterUserScreenNameUI.text = "User Screen Name: ";
                }
            }

            if (!string.IsNullOrEmpty(m_accessTokenResponse.Token))
            {
                m_twitterOnAccessErrorUI.text = "Access: Succedded";
            }
        }
        else
        {
            m_twitterUserScreenNameUI.text = "User Screen Name: ";
        }

        if (m_requestTokenResponse != null)
        {
            if (!string.IsNullOrEmpty(m_requestTokenResponse.Token))
            {
                if (m_twitterOnRequestErrorUI != null)
                {
                    m_twitterOnRequestErrorUI.text = "Request: Succedded";
                }
            }
            else
            {
                m_twitterOnRequestErrorUI.text = "Request: Failed";
            }
        }
    }

    protected virtual void InitializationCamera()
    {
        m_background = GameObject.Find("Camera Background").GetComponent<RawImage>();
        m_fit = GameObject.Find("Camera Background").GetComponent<AspectRatioFitter>();

        if (m_defaultBackground == null)
        {
            if (m_background != null)
            {
                m_defaultBackground = m_background.texture;
            }
        }

        WebCamDevice[] m_devices = WebCamTexture.devices;

        if (m_devices.Length == 0) 
        {
            if (m_cameraErrorUI != null)
            {
                m_cameraErrorUI.text = "No Camera";
            }
            m_camAvailable = false;
            return;
        }

        for (int i = 0; i < m_devices.Length; i++) 
        {
            if (!m_devices[i].isFrontFacing)
            {
                m_backCam = new WebCamTexture(m_devices[i].name, Screen.width, Screen.height);
            }
        }

        if (m_backCam == null)
        {
            if (m_cameraErrorUI != null)
            {
                m_cameraErrorUI.text = "Camera Not Found";
            }
            return;
        }

        if (m_cameraErrorUI != null)
        {
            m_cameraErrorUI.text = "";
        }

        m_backCam.Play();
        m_background.texture = m_backCam;

        m_camAvailable = true;
    }

    protected virtual void InitializationLocation()
    {
        StartCoroutine(StartLocationService());
    }

    protected virtual void InitializationTwitterUser()
    {
        m_accessTokenResponse = new Twitter.AccessTokenResponse();

        m_accessTokenResponse.UserId = PlayerPrefs.GetString(m_twitterUserID);
        m_accessTokenResponse.ScreenName = PlayerPrefs.GetString(m_twitterUserScreenName);
        m_accessTokenResponse.Token = PlayerPrefs.GetString(m_twitterUserToken);
        m_accessTokenResponse.TokenSecret = PlayerPrefs.GetString(m_twitterUserTokenSecret);

        m_requestTokenResponse = new Twitter.RequestTokenResponse();

        m_requestTokenResponse.Token = PlayerPrefs.GetString(m_requestToken);
        m_requestTokenResponse.TokenSecret = PlayerPrefs.GetString(m_requestTokenSecret);
    }

    #endregion

    // Update is called once per frame
    void Update ()
    {
        CameraUpdate();
        LocationUpdate();
        UIUpdate();
	}

    #region Application Updates

    protected virtual void CameraUpdate()
    {
        if (!m_camAvailable) 
        {
            if (m_cameraErrorUI != null)
            {
                m_cameraErrorUI.text = "Camera Not Avalible";
            }
            return;
        }

        if (m_cameraErrorUI != null)
        {
            m_cameraErrorUI.text = "";
        }

        float m_ratio = (float)m_backCam.width / (float)m_backCam.height;
        m_fit.aspectRatio = m_ratio;

        float m_scaleY = m_backCam.videoVerticallyMirrored ? -1f : 1f;
        m_background.rectTransform.localScale = new Vector3(1f, m_scaleY, 1f);

        int m_orient = -m_backCam.videoRotationAngle;
        m_background.rectTransform.localEulerAngles = new Vector3(0, 0, m_orient);
    }

    protected virtual void LocationUpdate()
    {
        if (Input.location.status == LocationServiceStatus.Running) 
        {
            m_latitude = Input.location.lastData.latitude;
            m_longitude = Input.location.lastData.longitude;
        }
    }

    protected virtual void UIUpdate()
    {
        if (m_latitudeUI != null)
        {
            m_latitudeUI.text = "LAT: " + m_latitude.ToString();
        }

        if (m_longitudeUI != null)
        {
            m_longitudeUI.text = "LON: " + m_longitude.ToString();
        }

        if (m_accessTokenResponse != null)
        {
            if (m_twitterUserScreenNameUI != null)
            {
                if (!string.IsNullOrEmpty(m_accessTokenResponse.ScreenName))
                {
                    m_twitterUserScreenNameUI.text = "User Screen Name: " + m_accessTokenResponse.ScreenName;
                }
                else
                {
                    m_twitterUserScreenNameUI.text = "User Screen Name: ";
                }
            }

            if (!string.IsNullOrEmpty(m_accessTokenResponse.Token)) 
            {
                m_twitterOnAccessErrorUI.text = "Access: Succedded";
            }
        }
        else
        {
            m_twitterUserScreenNameUI.text = "User Screen Name: ";
        }

        if (m_requestTokenResponse != null)
        {
            if (!string.IsNullOrEmpty(m_requestTokenResponse.Token))
            {
                if (m_twitterOnRequestErrorUI != null)
                {
                    m_twitterOnRequestErrorUI.text = "Request: Succedded";
                }
            }
        }
        else
        {
            m_twitterOnRequestErrorUI.text = "Request: Failed";
        }
    }

    #endregion

    #region Application Input

    public virtual void GetPin()
    {
        StartCoroutine(Twitter.API.GetRequestToken(m_consumerKey, m_consumerSecret, new Twitter.RequestTokenCallback(this.OnRequestTokenCallback)));
    }

    public virtual void EnterPin()
    {
        m_pin = m_pinUI.text.ToString();

        if (m_pin != null && m_requestTokenResponse != null)
        {
            StartCoroutine(Twitter.API.GetAccessToken(m_consumerKey, m_consumerSecret, m_requestTokenResponse.Token, m_pin, new Twitter.AccessTokenCallback(this.OnAccessTokenCallback)));
        }
        else
        {
            m_pinUI.text = "Please Eneter Pin";
        }
    }

    public virtual void UploadImage()
    {
        string m_messageText;

        m_messageText = "@YoureWatchedApp #YouAreBeingWatch at LAT: " + m_latitude.ToString() + " LON: " + m_longitude.ToString();
        StartCoroutine(CapturePhoto(m_messageText));
    }

    #endregion

    #region Application Twitter API

    protected virtual void OnRequestTokenCallback(bool m_success, Twitter.RequestTokenResponse m_response)
    {
        if (m_success)
        {
            if (m_twitterOnRequestErrorUI != null)
            {
                m_twitterOnRequestErrorUI.text = "Request: Succedded";
            }

            m_requestTokenResponse = m_response;

            PlayerPrefs.SetString(m_requestToken, m_requestTokenResponse.Token);
            PlayerPrefs.SetString(m_requestTokenSecret, m_requestTokenResponse.TokenSecret);
            PlayerPrefs.Save();

            Twitter.API.OpenAuthorizationPage(m_response.Token);
        }
        else
        {
            m_requestTokenResponse = null;

            if (m_twitterOnRequestErrorUI != null)
            {
                m_twitterOnRequestErrorUI.text = "Request: Failed";
            }
        }
    }

    protected virtual void OnAccessTokenCallback(bool m_success, Twitter.AccessTokenResponse m_response)
    {
        if (m_success)
        {
            if (m_twitterOnAccessErrorUI != null)
            {
                m_twitterOnAccessErrorUI.text = "Access: Succedded";
            }

            m_accessTokenResponse = m_response;

            PlayerPrefs.SetString(m_twitterUserID, m_response.UserId);
            PlayerPrefs.SetString(m_twitterUserScreenName, m_response.ScreenName);
            PlayerPrefs.SetString(m_twitterUserToken, m_response.Token);
            PlayerPrefs.SetString(m_twitterUserTokenSecret, m_response.TokenSecret);
            
            PlayerPrefs.Save();
        }
        else
        {
            m_accessTokenResponse = null;
            m_requestTokenResponse = null;

            PlayerPrefs.DeleteKey(m_twitterUserID);
            PlayerPrefs.DeleteKey(m_twitterUserScreenName);
            PlayerPrefs.DeleteKey(m_twitterUserToken);
            PlayerPrefs.DeleteKey(m_twitterUserTokenSecret);

            PlayerPrefs.DeleteKey(m_requestToken);
            PlayerPrefs.DeleteKey(m_requestTokenSecret);

            PlayerPrefs.Save();

            if (m_twitterOnAccessErrorUI != null)
            {
                m_twitterOnAccessErrorUI.text = "Access: Failed";
            }

            if (m_pinUI != null)
            {
                m_pinUI.text = "Please Eneter Pin";
            }
        }
    }

    protected virtual void OnTwitterUpload(bool m_success)
    {
        if (m_twitterOnPostErrorUI != null)
        {
            m_twitterOnPostErrorUI.text = "Post: " + (m_success ? "Succedded" : "Failed");
        }

    }

    #endregion

    #region Application Coroutine

    private IEnumerator StartLocationService()
    {
        if (!Input.location.isEnabledByUser)
        {
            if (m_locationErrorUI != null)
            {
                m_locationErrorUI.text = "Enable Location Service";
            }

            yield break;
        }

        Input.location.Start();
        int m_maxWait = 20;

        while (Input.location.status == LocationServiceStatus.Initializing && m_maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            m_maxWait--;
        }

        if (m_maxWait <= 0) 
        {
            if (m_locationErrorUI != null)
            {
                m_locationErrorUI.text = "Location Service Timeout";
            }

            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            if (m_locationErrorUI != null)
            {
                m_locationErrorUI.text = "Location Service Failed";
            }

            yield break;
        }

        if (m_locationErrorUI != null)
        {
            m_locationErrorUI.text = "";
        }

        m_latitude = Input.location.lastData.latitude;
        m_longitude = Input.location.lastData.longitude;

        yield break;
    }

    private IEnumerator CapturePhoto(string m_text)
    {
        yield return new WaitForEndOfFrame();

        if (m_camAvailable)
        {
            Texture2D m_photo = new Texture2D(m_backCam.width, m_backCam.height, TextureFormat.RGB24, false);
            m_photo.SetPixels(m_backCam.GetPixels());
            m_photo.Apply();
            
            //Texture2D m_photo = new Texture2D(m_background.texture.width, m_background.texture.height, TextureFormat.RGB24, false);
            //m_photo = (Texture2D)m_background.texture;

            byte[] m_tempBytes = m_photo.EncodeToPNG();

            if (m_tempBytes != null)
            {
                StartCoroutine(Twitter.API.PostTweet(m_text, m_tempBytes, m_consumerKey, m_consumerSecret, m_accessTokenResponse, new Twitter.PostTweetCallback(this.OnTwitterUpload)));
            }
        }
        else
        {
            Texture2D m_photo = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            m_photo.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            m_photo.Apply();

            byte[] m_tempBytes = m_photo.EncodeToPNG();

            File.WriteAllBytes(Application.dataPath + "/../test.png", m_tempBytes);

            if (m_tempBytes != null)
            {
                StartCoroutine(Twitter.API.PostTweet(m_text, m_tempBytes, m_consumerKey, m_consumerSecret, m_accessTokenResponse, new Twitter.PostTweetCallback(this.OnTwitterUpload)));
            }

        }
    }

    #endregion
}
